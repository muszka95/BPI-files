sbc-bench v0.6.7 Raspberry Pi 4 Model B Rev 1.1 (Mon, 08 Jul 2019 09:50:16 +0800)

Distributor ID:	Ubuntu
Description:	Ubuntu 19.04
Release:	19.04
Codename:	disco

Armbian release info:
BOARD=bananapi
BOARD_NAME="Banana Pi"
BOARDFAMILY=sun7i
BUILD_REPOSITORY_URL=https://github.com/armbian/build
BUILD_REPOSITORY_COMMIT=279dd0ac
VERSION=5.89
LINUXFAMILY=sunxi
BRANCH=dev
ARCH=arm
IMAGE_TYPE=user-built
BOARD_TYPE=conf
INITRD_ARCH=arm
KERNEL_IMAGE_TYPE=zImage

/usr/bin/gcc (Ubuntu/Linaro 8.3.0-6ubuntu1) 8.3.0

Uptime: 09:50:16 up 17 min,  1 user,  load average: 1.05, 0.74, 0.47

Linux 4.19.50-v7l+ (bpi-iot-ros-ai) 	07/08/19 	_armv7l_	(4 CPU)

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
          10.26    0.34    2.53    0.57    0.00   86.30

Device             tps    kB_read/s    kB_wrtn/s    kB_read    kB_wrtn
mmcblk0          15.89       508.48       343.42     520181     351320
zram0             2.81         0.71        10.53        728      10768
zram1             0.29         1.15         0.00       1176          4

              total        used        free      shared  buff/cache   available
Mem:          3.8Gi       157Mi       2.9Gi        23Mi       741Mi       3.6Gi
Swap:         1.5Gi          0B       1.5Gi

Filename				Type		Size	Used	Priority
/dev/zram1                             	partition	1048572	0	5
/var/swap                              	file    	558076	0	-2

##########################################################################

Checking cpufreq OPP:

Cpufreq OPP: 1500    Measured: 1497.907/1498.881/1499.055
Cpufreq OPP:  600    Measured: 597.404/599.123/595.924

##########################################################################

tinymembench v0.4.9 (simple benchmark for memory throughput and latency)

==========================================================================
== Memory bandwidth tests                                               ==
==                                                                      ==
== Note 1: 1MB = 1000000 bytes                                          ==
== Note 2: Results for 'copy' tests show how many bytes can be          ==
==         copied per second (adding together read and writen           ==
==         bytes would have provided twice higher numbers)              ==
== Note 3: 2-pass copy means that we are using a small temporary buffer ==
==         to first fetch data into it, and only then write it to the   ==
==         destination (source -> L1 cache, L1 cache -> destination)    ==
== Note 4: If sample standard deviation exceeds 0.1%, it is shown in    ==
==         brackets                                                     ==
==========================================================================

 C copy backwards                                     :   1826.5 MB/s
 C copy backwards (32 byte blocks)                    :   1826.2 MB/s
 C copy backwards (64 byte blocks)                    :   1826.7 MB/s
 C copy                                               :   1820.3 MB/s
 C copy prefetched (32 bytes step)                    :   1819.2 MB/s
 C copy prefetched (64 bytes step)                    :   1819.0 MB/s
 C 2-pass copy                                        :   1138.4 MB/s
 C 2-pass copy prefetched (32 bytes step)             :   1125.7 MB/s
 C 2-pass copy prefetched (64 bytes step)             :   1125.1 MB/s (0.2%)
 C fill                                               :   1875.5 MB/s (0.3%)
 C fill (shuffle within 16 byte blocks)               :   1873.0 MB/s (0.2%)
 C fill (shuffle within 32 byte blocks)               :   1875.1 MB/s (0.3%)
 C fill (shuffle within 64 byte blocks)               :   1876.5 MB/s (0.3%)
 ---
 standard memcpy                                      :   1020.2 MB/s
 standard memset                                      :   1875.4 MB/s (0.2%)
 ---
 NEON read                                            :   3622.9 MB/s
 NEON read prefetched (32 bytes step)                 :   3616.0 MB/s
 NEON read prefetched (64 bytes step)                 :   3616.1 MB/s
 NEON read 2 data streams                             :   3255.9 MB/s (0.3%)
 NEON read 2 data streams prefetched (32 bytes step)  :   3219.6 MB/s
 NEON read 2 data streams prefetched (64 bytes step)  :   3215.0 MB/s
 NEON copy                                            :   1819.1 MB/s
 NEON copy prefetched (32 bytes step)                 :   1817.5 MB/s
 NEON copy prefetched (64 bytes step)                 :   1818.3 MB/s
 NEON unrolled copy                                   :   1818.8 MB/s
 NEON unrolled copy prefetched (32 bytes step)        :   1817.5 MB/s
 NEON unrolled copy prefetched (64 bytes step)        :   1817.0 MB/s
 NEON copy backwards                                  :   1825.0 MB/s
 NEON copy backwards prefetched (32 bytes step)       :   1823.1 MB/s
 NEON copy backwards prefetched (64 bytes step)       :   1823.0 MB/s
 NEON 2-pass copy                                     :   1583.1 MB/s
 NEON 2-pass copy prefetched (32 bytes step)          :   1621.3 MB/s
 NEON 2-pass copy prefetched (64 bytes step)          :   1642.3 MB/s
 NEON unrolled 2-pass copy                            :   1547.7 MB/s
 NEON unrolled 2-pass copy prefetched (32 bytes step) :   1671.7 MB/s
 NEON unrolled 2-pass copy prefetched (64 bytes step) :   1663.8 MB/s
 NEON fill                                            :   1876.4 MB/s (0.5%)
 NEON fill backwards                                  :   1876.7 MB/s
 VFP copy                                             :   1820.8 MB/s (0.5%)
 VFP 2-pass copy                                      :   1485.2 MB/s
 ARM fill (STRD)                                      :   1875.0 MB/s (0.4%)
 ARM fill (STM with 8 registers)                      :   1875.8 MB/s (0.3%)
 ARM fill (STM with 4 registers)                      :   1876.5 MB/s (0.3%)
 ARM copy prefetched (incr pld)                       :   1817.5 MB/s
 ARM copy prefetched (wrap pld)                       :   1817.7 MB/s
 ARM 2-pass copy prefetched (incr pld)                :   1647.1 MB/s
 ARM 2-pass copy prefetched (wrap pld)                :   1605.7 MB/s

==========================================================================
== Memory latency test                                                  ==
==                                                                      ==
== Average time is measured for random memory accesses in the buffers   ==
== of different sizes. The larger is the buffer, the more significant   ==
== are relative contributions of TLB, L1/L2 cache misses and SDRAM      ==
== accesses. For extremely large buffer sizes we are expecting to see   ==
== page table walk with several requests to SDRAM for almost every      ==
== memory access (though 64MiB is not nearly large enough to experience ==
== this effect to its fullest).                                         ==
==                                                                      ==
== Note 1: All the numbers are representing extra time, which needs to  ==
==         be added to L1 cache latency. The cycle timings for L1 cache ==
==         latency can be usually found in the processor documentation. ==
== Note 2: Dual random read means that we are simultaneously performing ==
==         two independent memory accesses at a time. In the case if    ==
==         the memory subsystem can't handle multiple outstanding       ==
==         requests, dual random read has the same timings as two       ==
==         single reads performed one after another.                    ==
==========================================================================

block size : single random read / dual random read
      1024 :    0.0 ns          /     0.0 ns 
      2048 :    0.0 ns          /     0.0 ns 
      4096 :    0.0 ns          /     0.0 ns 
      8192 :    0.0 ns          /     0.0 ns 
     16384 :    0.0 ns          /     0.0 ns 
     32768 :    0.0 ns          /     0.0 ns 
     65536 :    5.7 ns          /     8.9 ns 
    131072 :    8.6 ns          /    11.9 ns 
    262144 :   12.3 ns          /    15.8 ns 
    524288 :   14.2 ns          /    18.1 ns 
   1048576 :   25.4 ns          /    38.4 ns 
   2097152 :   91.0 ns          /   135.5 ns 
   4194304 :  122.7 ns          /   164.4 ns 
   8388608 :  146.3 ns          /   189.5 ns 
  16777216 :  158.1 ns          /   200.7 ns 
  33554432 :  164.2 ns          /   206.6 ns 
  67108864 :  177.7 ns          /   223.5 ns 

##########################################################################

OpenSSL 1.1.1b, built on 26 Feb 2019
type             16 bytes     64 bytes    256 bytes   1024 bytes   8192 bytes  16384 bytes
aes-128-cbc      62064.17k    83089.13k    89821.61k    91547.65k    91876.01k    92018.01k
aes-128-cbc      68587.56k    82980.80k    89829.46k    91612.84k    92089.00k    92154.54k
aes-192-cbc      61010.46k    71926.53k    77128.96k    78323.37k    78542.17k    78632.28k
aes-192-cbc      61006.17k    72109.46k    77062.83k    78339.75k    78804.31k    78774.27k
aes-256-cbc      54858.42k    63823.89k    67522.47k    68470.78k    68719.96k    68714.50k
aes-256-cbc      54802.31k    63826.05k    67567.96k    68533.59k    68845.57k    68872.87k

##########################################################################

7-Zip (a) [32] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C,Utf16=off,HugeFiles=on,32 bits,4 CPUs LE)

LE
CPU Freq:  1490  1489  1498  1496  1499  1497  1498  1498  1498

RAM size:    3906 MB,  # CPU hardware threads:   4
RAM usage:    882 MB,  # Benchmark threads:      4

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       1272   100   1238   1238  |      22192   100   1894   1893
23:       1236   100   1260   1260  |      21732   100   1881   1880
24:       1186   100   1275   1275  |      21148   100   1857   1856
25:       1151   100   1315   1315  |      20452   100   1821   1820
----------------------------------  | ------------------------------
Avr:             100   1272   1272  |              100   1863   1863
Tot:             100   1568   1567

##########################################################################

7-Zip (a) [32] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C,Utf16=off,HugeFiles=on,32 bits,4 CPUs LE)

LE
CPU Freq:  1491  1496  1499  1498  1498  1496  1498  1498  1498

RAM size:    3906 MB,  # CPU hardware threads:   4
RAM usage:    882 MB,  # Benchmark threads:      4

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       3502   349    975   3407  |      87178   397   1874   7438
23:       3493   364    977   3560  |      85213   398   1852   7373
24:       3390   371    982   3646  |      82605   398   1823   7252
25:       3225   372    991   3683  |      79563   398   1780   7081
----------------------------------  | ------------------------------
Avr:             364    981   3574  |              398   1832   7286
Tot:             381   1407   5430

7-Zip (a) [32] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C,Utf16=off,HugeFiles=on,32 bits,4 CPUs LE)

LE
CPU Freq:  1497  1498  1498  1498  1498  1499  1498  1498  1498

RAM size:    3906 MB,  # CPU hardware threads:   4
RAM usage:    882 MB,  # Benchmark threads:      4

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       3528   351    979   3432  |      85785   392   1869   7319
23:       3461   361    978   3527  |      84628   396   1847   7322
24:       3298   359    987   3546  |      80948   391   1817   7106
25:       3176   365    994   3627  |      79522   398   1777   7077
----------------------------------  | ------------------------------
Avr:             359    984   3533  |              394   1828   7206
Tot:             377   1406   5370

7-Zip (a) [32] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C,Utf16=off,HugeFiles=on,32 bits,4 CPUs LE)

LE
CPU Freq:  1497  1497  1498  1498  1498  1498  1499  1497  1498

RAM size:    3906 MB,  # CPU hardware threads:   4
RAM usage:    882 MB,  # Benchmark threads:      4

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       3534   354    972   3438  |      87222   397   1873   7441
23:       3429   357    978   3494  |      84222   394   1847   7287
24:       3386   369    986   3642  |      82372   397   1821   7231
25:       3217   377    974   3674  |      78733   397   1765   7007
----------------------------------  | ------------------------------
Avr:             364    978   3562  |              396   1827   7242
Tot:             380   1402   5402

Compression: 3574,3533,3562
Decompression: 7286,7206,7242
Total: 5430,5370,5402

##########################################################################

Testing clockspeeds again. System health now:

Time        CPU    load %cpu %sys %usr %nice %io %irq   Temp
10:02:22:     MHz  3.48  86%   3%  83%   0%   0%   0%  80.3°C

Checking cpufreq OPP:

Cpufreq OPP: 1500    Measured: 1498.690/1499.055/1497.091
Cpufreq OPP:  600    Measured: 598.920/599.157/599.042

##########################################################################

System health while running tinymembench:

Time        CPU    load %cpu %sys %usr %nice %io %irq   Temp
09:50:17:     MHz  1.05  13%   2%  10%   0%   0%   0%  67.2°C
09:52:18:     MHz  1.00  25%   0%  25%   0%   0%   0%  67.7°C
09:54:18:     MHz  1.00  25%   0%  24%   0%   0%   0%  66.7°C

System health while running OpenSSL benchmark:

Time        CPU    load %cpu %sys %usr %nice %io %irq   Temp
09:55:28:     MHz  1.00  16%   1%  13%   0%   0%   0%  65.7°C
09:55:38:     MHz  1.00  25%   0%  25%   0%   0%   0%  67.2°C
09:55:48:     MHz  1.00  25%   0%  24%   0%   0%   0%  68.2°C
09:55:58:     MHz  1.00  25%   0%  25%   0%   0%   0%  68.7°C
09:56:08:     MHz  1.00  25%   0%  25%   0%   0%   0%  68.7°C
09:56:18:     MHz  1.00  25%   0%  25%   0%   0%   0%  67.7°C
09:56:28:     MHz  1.00  25%   0%  25%   0%   0%   0%  69.1°C
09:56:38:     MHz  1.00  25%   0%  25%   0%   0%   0%  68.2°C
09:56:48:     MHz  1.00  25%   0%  24%   0%   0%   0%  69.1°C
09:56:59:     MHz  1.00  25%   0%  25%   0%   0%   0%  69.6°C
09:57:09:     MHz  1.00  25%   0%  25%   0%   0%   0%  69.1°C

System health while running 7-zip single core benchmark:

Time        CPU    load %cpu %sys %usr %nice %io %irq   Temp
09:57:16:     MHz  1.00  17%   1%  14%   0%   0%   0%  68.2°C
09:58:16:     MHz  2.13  25%   0%  24%   0%   0%   0%  69.6°C
09:59:16:     MHz  2.83  25%   0%  24%   0%   0%   0%  68.7°C

System health while running 7-zip multi core benchmark:

Time        CPU    load %cpu %sys %usr %nice %io %irq   Temp
09:59:55:     MHz  3.03  17%   1%  15%   0%   0%   0%  69.1°C
10:00:16:     MHz  3.12  80%   2%  77%   0%   0%   0%  74.5°C
10:00:37:     MHz  3.00  89%   3%  86%   0%   0%   0%  75.0°C
10:00:58:     MHz  3.25  86%   2%  83%   0%   0%   0%  77.9°C
10:01:20:     MHz  3.60  90%   3%  86%   0%   0%   0%  78.9°C
10:01:41:     MHz  3.60  90%   3%  87%   0%   0%   0%  79.4°C
10:02:02:     MHz  3.59  81%   2%  79%   0%   0%   0%  80.8°C
10:02:22:     MHz  3.48  86%   3%  83%   0%   0%   0%  80.3°C

##########################################################################

Linux 4.19.50-v7l+ (bpi-iot-ros-ai) 	07/08/19 	_armv7l_	(4 CPU)

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
          21.99    0.20    1.85    0.33    0.00   75.63

Device             tps    kB_read/s    kB_wrtn/s    kB_read    kB_wrtn
mmcblk0           9.19       294.21       198.53     520677     351348
zram0             1.63         0.41         6.11        728      10816
zram1             0.17         0.66         0.00       1176          4

              total        used        free      shared  buff/cache   available
Mem:          3.8Gi       158Mi       2.9Gi        23Mi       742Mi       3.6Gi
Swap:         1.5Gi          0B       1.5Gi

Filename				Type		Size	Used	Priority
/dev/zram1                             	partition	1048572	0	5
/var/swap                              	file    	558076	0	-2

Architecture:        armv7l
Byte Order:          Little Endian
CPU(s):              4
On-line CPU(s) list: 0-3
Thread(s) per core:  1
Core(s) per socket:  4
Socket(s):           1
Vendor ID:           ARM
Model:               3
Model name:          Cortex-A72
Stepping:            r0p3
CPU max MHz:         1500.0000
CPU min MHz:         600.0000
BogoMIPS:            270.00
Flags:               half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32
